<?php

namespace CodeGeneratorBundle\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Sensio\Bundle\GeneratorBundle\Command\GenerateControllerCommand as BaseController;
use Sensio\Bundle\GeneratorBundle\Command\GeneratorCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Sensio\Bundle\GeneratorBundle\Command\Helper\QuestionHelper;
use CodeGeneratorBundle\Generator\ConsumerGenerator;

class GenerateConsumerCommand extends GeneratorCommand
{
    private $consumerName;
    private $bundleName;
    public function configure()
    {
        $this
            ->setDefinition(array(
                new InputOption('consumerName', '', InputOption::VALUE_REQUIRED, 'The name of the consumer to create'),
                new InputOption('bundleName', '', InputOption::VALUE_REQUIRED, 'The name of the bundle')
            ))
            ->addOption('consumerName', null, InputOption::VALUE_REQUIRED, '')
            ->addOption('bundleName', null, InputOption::VALUE_REQUIRED, '')
            ->setName('generate:consumer')
        ;
    }

    protected function createGenerator()
    {
        return new ConsumerGenerator($this->consumerName, $this->bundle, $this->getContainer()->get('filesystem'));
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $rootDir = $this->getContainer()->getParameter('kernel.root_dir');
        $this->consumerName=$input->getOption('consumerName');
        $this->bundleName=$input->getOption('bundleName');
        $this->bundle = $this->getContainer()->get('kernel')->getBundle($this->bundleName);
        $consumerGenerator = $this->getGenerator();
        $consumerGenerator->generate($this->bundle->getPath(), $rootDir);
        $output->writeln('Generating the bundle code: <info>OK</info>');
    }
}
