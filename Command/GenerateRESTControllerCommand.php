<?php

namespace CodeGeneratorBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Sensio\Bundle\GeneratorBundle\Command\GeneratorCommand;
use Symfony\Component\Console\Question\Question;
use Sensio\Bundle\GeneratorBundle\Command\Validators;
use CodeGeneratorBundle\Generator\RESTControllerGenerator;

class GenerateRESTControllerCommand extends GeneratorCommand
{
    /**
     * @see Command
     */
    public function configure()
    {
        $this->setDefinition(
            array(
                new InputOption(
                    'controller',
                    '',
                    InputOption::VALUE_REQUIRED,
                    'The name of the controller to create'
                ),
                new InputOption(
                    'type',
                    '',
                    InputOption::VALUE_REQUIRED,
                    'The type of the controller to create'
                ),
                new InputOption(
                    'action',
                    '',
                    InputOption::VALUE_REQUIRED,
                    'The type of the action to create'
                ),
                new InputOption(
                    'fields',
                    '',
                    InputOption::VALUE_REQUIRED,
                    'The fields of the resource.'
                )
            )
        )->setDescription('Generates a controller')
        ->setName('generate:RESTcontroller');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();

        if ($input->isInteractive()) {
            $question = new Question($questionHelper->getQuestion('Do you confirm generation', 'yes', '?'), true);
            if (!$questionHelper->ask($input, $output, $question)) {
                $output->writeln('<error>Command aborted</error>');

                return 1;
            }
        }

        if (null === $input->getOption('controller')) {
            throw new \RuntimeException('The controller option must be provided.');
        }

        list($bundle, $controller) = $this->parseShortcutNotation($input->getOption('controller'));
        if (is_string($bundle)) {
            $bundle = Validators::validateBundleName($bundle);

            try {
                $bundle = $this->getContainer()->get('kernel')->getBundle($bundle);
            } catch (\Exception $e) {
                $output->writeln(sprintf('<bg=red>Bundle "%s" does not exist.</>', $bundle));
            }
        }

        $questionHelper->writeSection($output, 'Controller generation');

        $generator = $this->getGenerator($bundle);
        $generator->generate(
            $bundle,
            $controller,
            $input->getOption('type'),
            $input->getOption('action'),
            $this->parseFields($input->getOption('fields'))
        );

        $output->writeln('Generating the bundle code: <info>OK</info>');

        $questionHelper->writeGeneratorSummary($output, array());
    }

    private function parseFields($input)
    {
        if (is_array($input)) {
            return $input;
        }

        $fields = array();
        foreach (explode(' ', $input) as $value) {
            $elements = explode(':', $value);
            $name = $elements[0];
            if (strlen($name)) {
                $type = isset($elements[1]) ? $elements[1] : 'string';
                preg_match_all('/(.*)\((.*)\)/', $type, $matches);
                $type = isset($matches[1][0]) ? $matches[1][0] : $type;
                $length = isset($matches[2][0]) ? $matches[2][0] : null;

                $fields[$name] = array('fieldName' => $name, 'type' => $type, 'length' => $length);
            }
        }

        return $fields;
    }

    public function interact(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();
        $questionHelper->writeSection($output, 'Welcome to the Symfony2 controller generator');

        // namespace
        $output->writeln(array(
            '',
            'Every page, and even sections of a page, are rendered by a <comment>controller</comment>.',
            'This command helps you generate them easily.',
            '',
            'First, you need to give the controller name you want to generate.',
            'You must use the shortcut notation like <comment>AcmeBlogBundle:Post</comment>',
            '',
        ));

        while (true) {
            $question = new Question(
                $questionHelper->getQuestion(
                    'Controller name',
                    $input->getOption('controller')
                ),
                $input->getOption('controller')
            );
            $question->setValidator(
                array(
                    'Sensio\Bundle\GeneratorBundle\Command\Validators',
                    'validateControllerName'
                )
            );
            $controller = $questionHelper->ask($input, $output, $question);
            list($bundle, $controller) = $this->parseShortcutNotation($controller);

            try {
                $b = $this->getContainer()->get('kernel')->getBundle($bundle);
                if (!file_exists($b->getPath() . '/Controller/' . $controller . 'Controller.php')) {
                    break;
                }
                $output->writeln(sprintf(
                    '<bg=red>Controller "%s:%s" already exists.</>',
                    $bundle,
                    $controller
                ));
            } catch (\Exception $e) {
                $output->writeln(sprintf('<bg=red>Bundle "%s" does not exist.</>', $bundle));
            }
        }
        $input->setOption('controller', $bundle . ':' . $controller);

        // summary
        $output->writeln(array(
            '',
            $this->getHelper('formatter')->formatBlock(
                'Summary before generation',
                'bg=blue;fg-white',
                true
            ),
            '',
            sprintf(
                'You are going to generate a "<info>%s:%s</info>" controller',
                $bundle,
                $controller
            ),
        ));
    }

    public function getPlaceholdersFromRoute($route)
    {
        preg_match_all('/{(.*?)}/', $route, $placeholders);
        $placeholders = $placeholders[1];

        return $placeholders;
    }

    public function parseShortcutNotation($shortcut)
    {
        $entity = str_replace('/', '\\', $shortcut);

        if (false === $pos = strpos($entity, ':')) {
            throw new \InvalidArgumentException(
                sprintf('The controller name must contain a : '.
                    '("%s" given, expecting something like AcmeBlogBundle:Post)', $entity)
            );
        }

        return array(substr($entity, 0, $pos), substr($entity, $pos + 1));
    }

    protected function createGenerator()
    {
        return new RESTControllerGenerator($this->getContainer()->get('filesystem'));
    }
}
