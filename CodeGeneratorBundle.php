<?php

namespace CodeGeneratorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CodeGeneratorBundle extends Bundle
{
    public function getParent()
    {
        return 'SensioGeneratorBundle';
    }
}
