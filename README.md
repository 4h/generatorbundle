This is a symfony's sensio generate bundle's extended bundle.

You can generate three things using this generator bundle, it works like sensio generator bundle via command line.

1. To create consumer run this command:

` php app/console generate:consumer --no-interaction --bundleName=[bundleName] --consumerName=[consumerName] `

Example:

` consumer --no-interaction --bundleName=QuoteWriteBundle --consumerName=Quote `

2. To create controller run this command:

` php app/console generate:RESTcontroller --no-interaction --controller=bundleName:controllerName --type=type  --action=getall --fields="fields" `

Example:

php app/console generate:RESTcontroller --no-interaction --controller=QuoteWriteBundle:Quote --type=write  --action=putevent --fields="guid:string tenderGuid:string author:string price:float "


3. To create document run this command:

` php app/console generate:doctrine:document --no-interaction --document=bundleName:documentName --fields="fields" --format=yml `

Example:

` php app/console generate:doctrine:document --no-interaction --document=QuoteWriteBundle:Quote --fields="guid:string tenderGuid:string author:string price:float " --format=yml `
