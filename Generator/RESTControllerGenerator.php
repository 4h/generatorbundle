<?php

namespace CodeGeneratorBundle\Generator;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

class RESTControllerGenerator extends Generator
{
    private $filesystem;

    /**
     * Constructor.
     * @param Filesystem $filesystem A Filesystem instance
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function generate(
        BundleInterface $bundle,
        $controller,
        $type,
        $action,
        $fields
    ) {
        $dir = $bundle->getPath();
        $controllerFile = $dir.'/Controller/'.$controller.$action.'Controller.php';
        if (file_exists($controllerFile)) {
            throw new \RuntimeException(sprintf('Controller "%s" already exists', $controller));
        }
        $baseControllerFile = $dir.'/Controller/'.$controller.'BaseController.php';
        $controllerLowercase = lcfirst($controller);
        $controllerUnderscored = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $controller));
        $fieldsJSON = json_encode($fields);
        $fieldKeys = [];
        $fieldKeysWithData = [];
        foreach ($fields as $fieldName => $fieldValue) {
            $fieldKeys[] = '$'.$fieldName;
            $fieldKeysWithData[] = '$data->'.$fieldName;
        }
        $parameters = array(
            'namespace'  => $bundle->getNamespace(),
            'bundle'     => $bundle->getName(),
            'controller' => $controller,
            'controllerLowercase' => $controllerLowercase,
            'controllerUnderscored' => $controllerUnderscored,
            'type' => $type,
            'action' => $action,
            'fieldsJSON' => $fieldsJSON,
            'fieldKeys' => $fieldKeys,
            'fieldKeysWithData' => $fieldKeysWithData
        );

        $this->setSkeletonDirs(__DIR__."/../Resources/skeleton");
        $this->renderFile(
            "controller/REST_{$type}_{$action}_Controller.php.twig",
            $controllerFile,
            $parameters
        );
        if (!file_exists($baseControllerFile)) {
            $this->renderFile(
                "controller/REST_base_Controller.php.twig",
                $baseControllerFile,
                $parameters
            );
        }
        $this->renderFile(
            "tests/REST_{$type}_{$action}_ControllerTestFunctional.php.twig",
            $dir.'/Tests/Functional/'.$controller.$action.'ControllerTest.php',
            $parameters
        );
        $this->addRoute($bundle, $controller, $action);
        $this->addService($bundle, $controller, $action);
    }

    protected function addRoute($bundle, $controller, $action)
    {
        $yaml = new Parser();
        $bundleRoute = $yaml->parse(\file_get_contents($bundle->getPath().'/Resources/config/routing.yml'));
        $controllerUnderscored = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $controller.$action));
        $bundleRoute[$controllerUnderscored]['type'] = 'rest';
        $bundleRoute[$controllerUnderscored]['resource'] = $controllerUnderscored.'_controller';
        $dumper = new Dumper();
        \file_put_contents($bundle->getPath().'/Resources/config/routing.yml', $dumper->dump($bundleRoute, 2));
    }

    protected function addService($bundle, $controller, $action)
    {
        $yaml = new Parser();
        $bundleServices = $yaml->parse(\file_get_contents($bundle->getPath().'/Resources/config/services.yml'));
        $controllerUnderscored = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $controller.$action));
        $bundleServices['services'][$controllerUnderscored.'_controller']['class'] =
            $bundle->getNamespace().'\\Controller\\'.$controller.$action.'Controller';
        $bundleServices['services'][$controllerUnderscored.'_controller']['arguments'][0] =
            '@doctrine_mongodb';
        $bundleServices['services'][$controllerUnderscored.'_controller']['arguments'][1] =
            "@=service('request_stack')";
        $bundleServices['services'][$controllerUnderscored.'_controller']['arguments'][2] =
            '@service_container';
        $dumper = new Dumper();
        \file_put_contents(
            $bundle->getPath().'/Resources/config/services.yml',
            $dumper->dump($bundleServices, 3)
        );
    }

    protected function parseTemplatePath($template)
    {
        $data = $this->parseLogicalTemplateName($template);
        return $data['controller'].'/'.$data['template'];
    }

    protected function parseLogicalTemplateName($logicalname, $part = '')
    {
        $data = array();
        list($data['bundle'], $data['controller'], $data['template']) = explode(':', $logicalname);
        return ($part ? $data[$part] : $data);
    }
}
