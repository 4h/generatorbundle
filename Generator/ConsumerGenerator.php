<?php

namespace CodeGeneratorBundle\Generator;

use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;

class ConsumerGenerator extends Generator
{
    private $bundle;
    private $consumerName;
    private $filesystem;
    public function __construct($consumerName, $bundle, Filesystem $filesystem)
    {

        $this->bundle = $bundle;
        $this->consumerName = $consumerName;
        $this->filesystem = $filesystem;
    }

    public function generate($path, $rootDir)
    {
        $this->setSkeletonDirs(__DIR__."/../Resources/skeleton");
        $consumerNameUnderscored = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $this->consumerName));
        $this->renderFile('consumer/Consumer.php.twig',
            $path.'/Consumer/'.ucfirst($this->consumerName).'Consumer.php',
            array('consumerName'=>ucfirst($this->consumerName),
                'consumerNameUnderscored'=>$consumerNameUnderscored,
                'bundleName' => ucfirst($this->bundle->getName()),
                'namespace'=> $this->bundle->getNamespace()
            )
        );
        $this->renderFile('tests/ConsumerTestFunctional.php.twig',
            $path.'/Tests/Functional/'.ucfirst($this->consumerName).'ConsumerTest.php',
            array('consumerName'=>ucfirst($this->consumerName),
                'bundleName' => ucfirst($this->bundle->getName()),
                'namespace' => $this->bundle->getNamespace()
            )
        );
        $this->renderFile('configs/services.yml.twig',
            $path.'/Resources/config/services.yml',
            array('consumerName'=>ucfirst($this->consumerName),
                'consumerNameUnderscored'=>$consumerNameUnderscored,
                'bundleName' => ucfirst($this->bundle->getName()),
                'namespace' => $this->bundle->getNamespace()
            )
        );
        $consumerConfig = $this->render('configs/config.yml.twig',
            array('consumerName'=>ucfirst($this->consumerName),
                'consumerNameUnderscored'=>$consumerNameUnderscored,
                'bundleName' => ucfirst($this->bundle->getName())
            )
        );
        $yaml = new Parser();
        $configYml = $yaml->parse(file_get_contents($rootDir.'/config/config.yml'));
        $dumper = new Dumper();
        $consumerConfig = $yaml->parse($consumerConfig);
        $configYml["old_sound_rabbit_mq"] += $consumerConfig;
        $configYml = $dumper->dump($configYml, 10);
        file_put_contents ( $rootDir.'/config/config.yml', $configYml);
    }
}
