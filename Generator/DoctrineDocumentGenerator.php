<?php

namespace CodeGeneratorBundle\Generator;

use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;
use Doctrine\ODM\MongoDB\Tools\DocumentGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use CodeGeneratorBundle\Exporter\YamlExporter;

class DoctrineDocumentGenerator extends Generator
{
    private $filesystem;
    private $registry;

    public function __construct(Filesystem $filesystem, RegistryInterface $registry)
    {
        $this->filesystem = $filesystem;
        $this->registry = $registry;
    }

    public function generate(BundleInterface $bundle, $document, $format, array $fields, $withRepository)
    {
        // configure the bundle (needed if the bundle does not contain any Entities yet)
        $config = $this->registry->getManager(null)->getConfiguration();
        $config->setEntityNamespaces(array_merge(
            array($bundle->getName() => $bundle->getNamespace().'\\Document'),
            $config->getEntityNamespaces()
        ));

        $documentClass = $this->registry->getAliasNamespace($bundle->getName()).'\\'.$document;
        $documentPath = $bundle->getPath().'/Document/'.str_replace('\\', '/', $document).'.php';
        if (file_exists($documentPath)) {
            throw new \RuntimeException(sprintf('Document "%s" already exists.', $documentClass));
        }

        $class = new ClassMetadataInfo($documentClass);
        if ($withRepository) {
            $class->customRepositoryClassName = $documentClass.'Repository';
        }
        $class->mapField(array('fieldName' => 'id', 'type' => 'integer', 'id' => true));
        $class->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_AUTO);
        foreach ($fields as $field) {
            $class->mapField($field);
        }

        $documentGenerator = $this->getDocumentGenerator();
        if ('annotation' === $format) {
            $documentGenerator->setGenerateAnnotations(true);
            $documentCode = $documentGenerator->generateDocumentClass($class);
            $mappingPath = $mappingCode = false;
        } else {
            $exporter = new YamlExporter();
            $mappingPath = $bundle->getPath().'/Resources/config/doctrine/'.str_replace('\\', '.', $document).'.mongodb.'.$format;

            if (file_exists($mappingPath)) {
                throw new \RuntimeException(sprintf('Cannot generate document when mapping "%s" already exists.', $mappingPath));
            }
            $mappingCode = $exporter->exportClassMetadata($class);
            $documentGenerator->setGenerateAnnotations(false);
            $documentCode = $documentGenerator->generateDocumentClass($class);
	    
            $constructor =  "\n".str_repeat(' ', 4).'/**'. "\n".
                            str_repeat(' ', 4).'* @param $guid type string users guid'."\n".
                            str_repeat(' ', 4).'*/'."\n".
                            str_repeat(' ', 4).'public function __construct($guid)'."\n".
                            str_repeat(' ', 4).'{'."\n".
                            str_repeat(' ', 8).'$this->guid = $guid;'."\n".
                            str_repeat(' ', 4).'}'."\n";

            $toArray =  "\n".str_repeat(' ', 4).'/**'. "\n".
                        str_repeat(' ', 4).'* This method converts entity to array with approprite key values'."\n".
                        str_repeat(' ', 4).'* @return type'."\n".
                        str_repeat(' ', 4).'*/'."\n".
                        str_repeat(' ', 4).'public function toArray()'."\n".
                        str_repeat(' ', 4).'{'."\n".
                        str_repeat(' ', 8).'$array = array();'."\n".
                        str_repeat(' ', 8).'$array["guid"] = $this->guid;'."\n".
                        str_repeat(' ', 8).'return $array;'."\n".
                        str_repeat(' ', 4).'}'."\n";

            $documentCode = substr_replace($documentCode, $constructor.$toArray, strlen($documentCode)-1, 0);

        }

        $this->filesystem->mkdir(dirname($documentPath));
        file_put_contents($documentPath, $documentCode);
        $this->addBundleToConfig($bundle);
        if ($mappingPath) {
            $this->filesystem->mkdir(dirname($mappingPath));
            file_put_contents($mappingPath, $mappingCode);
        }

        if ($withRepository) {
            $path = $bundle->getPath().str_repeat('/..', substr_count(get_class($bundle), '\\'));
            $this->getRepositoryGenerator()->writeDocumentRepositoryClass($class->customRepositoryClassName, $path);
        }
    }
    
    protected function addBundleToConfig($bundle)
    {
        $yaml = new Parser();
        $configs = $yaml->parse(\file_get_contents($bundle->getPath().'/../../../app/config/config.yml'));
        $configs['doctrine_mongodb']['document_managers']['default']['mappings'][$bundle->getName()]['type'] = 'yml';
        $configs['doctrine_mongodb']['document_managers']['default']['mappings'][$bundle->getName()]['dir'] = 'Resources/config/doctrine';
        $dumper = new Dumper();
        \file_put_contents($bundle->getPath().'/../../../app/config/config.yml', $dumper->dump($configs, 100));
    }

    public function isReservedKeyword($keyword)
    {
        return $this->registry->getConnection()->getDatabasePlatform()
            ->getReservedKeywordsList()->isKeyword($keyword);
    }

    protected function getDocumentGenerator()
    {
        $documentGenerator = new DocumentGenerator();
        $documentGenerator->setGenerateAnnotations(false);
        $documentGenerator->setGenerateStubMethods(true);
        $documentGenerator->setRegenerateDocumentIfExists(false);
        $documentGenerator->setUpdateDocumentIfExists(true);
        $documentGenerator->setNumSpaces(4);
//        $documentGenerator->setAnnotationPrefix('ORM\\'); 

        return $documentGenerator;
    }

    protected function getRepositoryGenerator()
    {
        return new DocumentRepositoryGenerator();
    }
}
