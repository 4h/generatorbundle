<?php

namespace CodeGeneratorBundle\Exporter;

use Symfony\Component\Yaml\Yaml;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;

class YamlExporter
{
    /**
     * @var string
     */
    protected $_extension = '.dcm.yml';

    /**
     * {@inheritdoc}
     */
    public function exportClassMetadata(ClassMetadataInfo $metadata)
    {
        $array = array();

        $fieldMappings = $metadata->fieldMappings;

        if ($fieldMappings) {
            if ( ! isset($array['fields'])) {
                $array['fields'] = array();
            }
            foreach($fieldMappings as $fieldname => $fieldContent) {
                $array['fields'][$fieldname]['type'] = $fieldContent['type'];
            }
        }
	$array['fields']['id']['id']=TRUE;
	unset($array['fields']['id']['type']);
        return Yaml::dump(array($metadata->name => $array), 10);
    }
}
